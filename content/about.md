+++
title = "About"
date = "2020-05-17"
menu = "main"
+++

NeoBabel is a system agnostic hard sci-fi world intended to be released under
the Creative Commons Zero license to allow free manipulation by others, and
an infinite number of use cases. It is primarily intended to be a world setting
for use in tabletop roleplaying games.

The purpose of NeoBabel is to create a world where players can be dropped in
at any point along the history of said world and have enough information and
resources to play any style campaign they may want.

The setting, and the theme within, is deliberately left vague so as to leave
options available to both the GM and the players. If the players are interested
in trying to survive day to day in a hostile dystopian world, that should be
possible. If they would rather function at the higher levels, seeking to make
a monumental impact upon the systems in the world, that should be possible
as well. It should even be possible for the players to participate in the
dystopia itself, as corporate leaders, government representatives, or shadowy
power brokers.

Of course, the author has implemented certain design decisions you should be
aware of.

If you are looking for a world where magic exists, or psychic abilities, this
is likely not going to be your setting. A player may be able to _fool_ someone
into believing they are magical, after all "sufficiently advanced technology
is indistinguishable from magic", but this will require careful work and
technical skill to pull off well. A player may be able to play off that they
are "psionically capable of hearing others" when in fact they have carefully
established a back door into target cellphones and modified their own
[Babel Implant][1] to be able to listen in at will.

Extraordinary things should _appear_ possible, but there should always be a
logical explanation behind them, and the characters should thus, with sufficient
resources, be able to demonstrate exceptional power -- if they are clever enough
to sell mundane explanations as such.

[1]: /technology/babel/
