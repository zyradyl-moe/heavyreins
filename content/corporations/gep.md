---
date: 2020-05-17T15:45:00Z
lastmod: 2020-05-18T15:45:00Z
draft: false
weight: 20

title: Global Engineering And Protection Corporation
description: "We make the World."
---

### "We make the world." ###

## History ##

### Rise of CACEPS and Laminar Cities ###

The Global Engineering and Protection Corporation started its existence as the
Global Engineering and Protection Project, inside the umbrella of the
[Unified Human Government's][1] [Humanity's Survival Initiative][2]. A group of
scientists tasked with the unenviable responsibility of developing a way to
stabilize the climate in a given area, it quickly became one of the most
resource hungry of the MegaProjects. [Man-made Climate Change][3] had merged with
the atmospheric destruction of [The Last War][4] to produce an environment so
unstable that meteorologists were no longer able to algorithmically predict
what would happen each day.

Eventually, in <span style="color:red">????</span> there was a breakthrough.
Through a combination of cloud seeding[^1], laser guided discharge of
electrical potential[^2], utilization of carbon nanoparticles[^3], and massive
computational power[^4], the GEPP Scientists announced the creation of the
[Climate Analysis, Control, Engineering, and Protection System][5].

The system was monumental in scale, incredibly complex, incredibly expensive,
and was limited in its range. The UHG initially resisted the solution, insisting
there had to be a planet wide option. Finally, the UHG sent back a mandate to
GEPP: Either make the system planet wide, or figure out a way to consolidate the
human population.

In <span style="color:red">????</span> GEPP once again produced a viable option.
Dubbed "[Laminar Cities][6]" due to their appearance and utilitarian design,
GEPP had designed massive metropolises that were perfectly symmetrical,
perfectly equal, and designed to minimize differences in living environments.

As a positive side effect, the cities would allow any storm systems that did
manage to bypass CACEPS to pass right through with minimal resistance, and may
even help to break the storm up.

Such an egalitarian solution strongly appealed to the UHG of the time, and in
<span style="color:red">????</span> the
[Regional Unification and Modernization Initiative][7] was signed into law,
signaling the start of the largest series of construction projects in human
history.

### AI Controversy & Proposition EAICAR ###

During the initial UHG Conferences on CACEPS, the fact that the system would not
work planet wide provided enough of a distraction to prevent the UHG
Representatives from probing at a potentially sore spot of the CACEP System,
namely the fact that no human or team of humans was anywhere near fast enough
to process all the analysis data and control the vast array of drones and
satellites with the precision that GEPP was advertising.

Since the Committees didn't ask, the scientists didn't see any reason to
announce the fact that they were using [Artificial Intelligence][8] to control
the system. Between the vast array of sensors, drone aircraft, satellites,
computer control modules, and the massively complex calculations needed to
account for entropy, a team of humans didn't stand a chance at doing more than
staving off the very worst. An artificial intelligence however, could.

Their secret remained safe through the construction of all the cities, and
through the initial relocation of the global population. All seemed like it
would go perfectly to plan, and by the time any information had leaked, people
would be so comfortable it wouldn't matter.

Then, in <span style="color:red">????</span> a whistleblower announced the
secret behind CACEPS during primetime on a Global News Network. By the next
morning, the global population was a hair's breadth away from a full scale riot.
Everyone knew the official story of the AI's disobedience during The Last War,
and the idea of this same type of disobedient intelligence being in control of
their very survival was simply too much to handle for the majority of the world.

Not content to allow the normal procedures and investigations to play out,
people called for a [Unified Human Election][9] on a single proposition.
The proposition's language was fleshed out within 24 hours, calming the
population, and barely more than one hundred hours after the news first broke,
[Eliminating Artificial Intelligence in Climate Analysis and Regulation][10]
went to a vote.

GEPP scientists watched in horror as, in a landslide, the proposal passed. They
were given fourteen days to develop a plan to move away from AI intervention,
and 90 days to implement it.

### The Human Era of Climate Engineering ###

As expected, once AI had been removed from the system, its performance dropped
dramatically. Human operators were only able to prevent 75% of the extreme
storm category, and 40% of the moderate storm category. In deserts, massive
dust storms being channeled through the Laminar streets became common. Along
coastlines, a new cottage industry sprang up helping people to put Hurricane
Shutters on their apartments.

Rather than attempt to prevent everything, the human operators quickly learned
to only focus their time on potentially lethal storm systems, and within a few
years almost every city would experience a minimum of two storm systems a day.

Surprisingly, the population didn't actually mind. Alarm systems were developed,
meteorologists suddenly learned to read the CACEPS output, and the world
normalized into a human controlled, albeit stormy, climate lifestyle.

### CASH Act & Independence ###

Severed from their governmental dependence by the [CASH Act][11] in
<span style="color:red">????</span>, GEPP was spun off to become GEP
Corporation, but interestingly, not much changed for the Scientists involved.

Sure they had to report to a board of directors, their expenses were intently
monitored, and their salaries had shifted, but the severing from a government
source of income had all but ensured that another Laminar City could never be
created. As the existing Laminar Cities were still technically regulated by
the government, the idea of private citizenship, at least for GEP Co, was a
foreign one.

Their income was primarily in the form of government subsidies, and their
expenses were the same as they were previously. The end result is that GEP
Co. functions as a sort of privately controlled government department.

## Products ##

 * [Climate Analysis, Control, Engineering, and Protection System][5]
 * [Laminar Cities][6]

## Notable Employees ##

To Be Developed.

## Important Dates ##

To Be Developed.

## Corporate Headquarters ##

To Be Developed.

## Author Notes & Meta Information ##

Except for the single incident involving Artificial Intelligence, the public is
actually pretty comfortable with GEP Co's public image. They stick to their
domain quite well, and they've become fairly transparent with the information
they release. Realtime CACEPS data for areas both inside and outside the sphere
of influence is freely available to any tax-paying member of the public, and
they've even volunteered to help train individuals from the local news agencies
on how to interpret it.

Internally, GEP Corporation is one of the few that doesn't have a large hunger
for power. Their domain is fairly stable, and the board of directors typically
view their stewardship as a type of community service to the world, and as such
they aren't typically interested in milking the corporation for every dime.

However, within the Scientific Directors, there is a large group of individuals
trying to gain slight influence over [HALOC's][12] [HAARPS][13] curriculum.
These scientists reject the official account of AI's role in The Last War, and
believe that the Government is withholding their actual role in order to make
humanity look better. They believe that a slow shift in HAARPS curriculum could
positively influence the population to vote to allow them access to AI once
again, which would solve the current storm issue.



[^1]: https://en.wikipedia.org/wiki/Cloud_seeding
[^2]: http://cogentbenger.com/documentaries/how-to-stop-a-hurricane/
[^3]: Gray, W.M., W.M. Frank, M.L. Corrin, C.A. Stokes (1976): "Weather modification by carbon dust absorption of solar energy" J. Appl. Meteor., 15, pp.355-386
[^4]: http://www.niac.usra.edu/files/library/meetings/annual/jun02/715Hoffman.pdf

[1]: /government/uhg/
[2]: /government/hsi/
[3]: /history/climatechange/
[4]: /history/lastwar/
[5]: /technology/caceps/
[6]: /technology/laminar/
[7]: /laws/rumi/
[8]: /technology/ai/
[9]: /government/uhe/
[10]: /laws/eaicar/
[11]: /laws/cash/
[12]: /corporations/haloc/
[13]: /laws/haarps/
